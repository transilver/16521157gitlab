#include "FillColor.h"
#include <iostream>
#include <stack>
using namespace std;

#define INT_SIZE sizeof(Uint32) * 8 /* Integer size in bits */

int findHighestBitSet(Uint32 num)
{
	int order = 0, i;

	/* Iterate over each bit of integer */
	for (i = 0; i<INT_SIZE; i++)
	{
		/* If current bit is set */
		if ((num >> i) & 1)
			order = i;
	}

	return order;
}

//Get color of a pixel
SDL_Color getPixelColor(Uint32 pixel_format, Uint32 pixel)
{
	SDL_PixelFormat* fmt = SDL_AllocFormat(pixel_format);

	Uint32 temp;
	Uint8 red, green, blue, alpha;

	//Check if pixel is a 32-bit integer
	if (findHighestBitSet(pixel) == 31)
	{
		/* Get Alpha component */
		temp = pixel & fmt->Amask;  /* Isolate alpha component */
		temp = temp >> fmt->Ashift; /* Shift it down to 8-bit */
		temp = temp << fmt->Aloss;  /* Expand to a full 8-bit number */
		alpha = (Uint8)temp;
	}
	else {
		alpha = 255;
	}

	/* Get Red component */
	temp = pixel & fmt->Rmask;  /* Isolate red component */
	temp = temp >> fmt->Rshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Rloss;  /* Expand to a full 8-bit number */
	red = (Uint8)temp;

	/* Get Green component */
	temp = pixel & fmt->Gmask;  /* Isolate green component */
	temp = temp >> fmt->Gshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Gloss;  /* Expand to a full 8-bit number */
	green = (Uint8)temp;

	/* Get Blue component */
	temp = pixel & fmt->Bmask;  /* Isolate blue component */
	temp = temp >> fmt->Bshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Bloss;  /* Expand to a full 8-bit number */
	blue = (Uint8)temp;


	SDL_Color color = { red, green, blue, alpha };
	return color;

}


//Get all pixels on the window
SDL_Surface* getPixels(SDL_Window* SDLWindow, SDL_Renderer* SDLRenderer) {
	SDL_Surface* saveSurface = NULL;
	SDL_Surface* infoSurface = NULL;
	infoSurface = SDL_GetWindowSurface(SDLWindow);
	if (infoSurface == NULL) {
		std::cerr << "Failed to create info surface from window in saveScreenshotBMP(string), SDL_GetError() - " << SDL_GetError() << "\n";
	}
	else {
		unsigned char * pixels = new (std::nothrow) unsigned char[infoSurface->w * infoSurface->h * infoSurface->format->BytesPerPixel];
		if (pixels == 0) {
			std::cerr << "Unable to allocate memory for screenshot pixel data buffer!\n";
			return NULL;
		}
		else {
			if (SDL_RenderReadPixels(SDLRenderer, &infoSurface->clip_rect, infoSurface->format->format, pixels, infoSurface->w * infoSurface->format->BytesPerPixel) != 0) {
				std::cerr << "Failed to read pixel data from SDL_Renderer object. SDL_GetError() - " << SDL_GetError() << "\n";
				delete[] pixels;
				return NULL;
			}
			else {
				saveSurface = SDL_CreateRGBSurfaceFrom(pixels, infoSurface->w, infoSurface->h, infoSurface->format->BitsPerPixel, infoSurface->w * infoSurface->format->BytesPerPixel, infoSurface->format->Rmask, infoSurface->format->Gmask, infoSurface->format->Bmask, infoSurface->format->Amask);
			}
			delete[] pixels;
		}
	}
	return infoSurface;
}

//Get pixel
Uint32 get_pixel32(SDL_Surface *surface, int x, int y)
{
	//Convert the pixels to 32 bit
	Uint32 *pixels = (Uint32 *)surface->pixels;

	//Get the requested pixel
	return pixels[(y * surface->w) + x];
}

//Put pixel
void put_pixel32(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
	//Convert the pixels to 32 bit
	Uint32 *pixels = (Uint32 *)surface->pixels;

	//Set the pixel
	pixels[(y * surface->w) + x] = pixel;
}

//Compare two colors
bool compareTwoColors(SDL_Color color1, SDL_Color color2)
{
	if (color1.r == color2.r && color1.g == color2.g && color1.b == color2.b && color1.a == color2.a)
		return true;
	return false;
}

bool checkpainted(SDL_Window *win, Vector2D startPoint, Uint32 pixel_format,
	SDL_Renderer *ren, SDL_Color fillColor, SDL_Color boundaryColor)
{
	SDL_Color color = getPixelColor(pixel_format, get_pixel32(getPixels(win, ren), startPoint.x, startPoint.y));
	color.a = 255;
	if ((compareTwoColors(color, boundaryColor) == true)||(compareTwoColors(color, fillColor) == true))	return true;
	return false;
}

bool canFilled(SDL_Window *win, Vector2D newPoint, Uint32 pixel_format,
	SDL_Renderer *ren, SDL_Color fillColor, SDL_Color boundaryColor)
{
	SDL_Surface *surface = getPixels(win, ren);

	//Convert the pixels to 32 bit
	Uint32 *pixels = (Uint32 *)surface->pixels;
	int w = surface->w;

	int index = newPoint.y * w + newPoint.x;
	Uint32 pixel = pixels[index];
	SDL_Color color = getPixelColor(pixel_format, pixel);
	cout << (int)color.r << "," << (int)color.g << "," << (int)color.b << "," << (int)color.a << endl;

	if (!compareTwoColors(color, fillColor) && !compareTwoColors(color, boundaryColor))
	{
		return true;
	}

	return false;
}

void BoundaryFill4(SDL_Window *win, Vector2D startPoint, Uint32 pixel_format,
	SDL_Renderer *ren, SDL_Color fillColor, SDL_Color boundaryColor)
{
	stack<Vector2D> s;
	Vector2D t;
	t.x = startPoint.x, t.y = startPoint.y;
	s.push({ t.x,t.y });
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	while (s.empty()==false)
	{
		t = s.top();
		SDL_RenderDrawPoint(ren, t.x, t.y);
		//Left
		bool p = checkpainted(win, { t.x - 1,t.y }, pixel_format, ren, fillColor, boundaryColor);
		if ( p == false)
		{
			s.push({ t.x - 1 ,t.y });
			continue;
		}
		//Right
		p = checkpainted(win, { t.x + 1,t.y }, pixel_format, ren, fillColor, boundaryColor);
		if ( p == false)
		{
			s.push({ t.x + 1 ,t.y });
			continue;
		}
		//Top
		p = checkpainted(win, { t.x, t.y - 1 }, pixel_format, ren, fillColor, boundaryColor);
		if ( p == false)
		{
			s.push({ t.x, t.y - 1 });
			continue;
		}
		//Bottom
		p = checkpainted(win, { t.x, t.y + 1 }, pixel_format, ren, fillColor, boundaryColor);
		if ( p == false)
		{
			s.push({ t.x, t.y + 1 });
			continue;
		}
		s.pop();
	}
	return;
}

//======================================================================================================================
//=============================================FILLING TRIANGLE=========================================================

int maxIn3(int a, int b, int c)
{
	if (a < b)
	{
		if (b < c) return c;
		return b;
	}
	else if (a > c) return a;
	return c;
}

int minIn3(int a, int b, int c)
{
	if (a > b)
	{
		if (b > c)return a;
		return b;
	}
	else if (a < c)return a;
	return c;
}

void swap(Vector2D &a, Vector2D &b)
{
	Vector2D temp=a;
	a = b;
	b = temp;
	return;
}

void ascendingSort(Vector2D &v1, Vector2D &v2, Vector2D &v3)
{
	if (v1.y <= v2.y)
	{
		if (v2.y <= v3.y) return;
		swap(v2, v3);
		return;
	}
	else
	{
		swap(v1, v2);
		if (v2.y <= v3.y) return;
		if (v3.y > v1.y) swap(v2, v3);
		else { swap(v1, v3); swap(v3, v2); }
	}
	
}

void TriangleFill1(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	int Min = minIn3(v1.x, v2.x, v3.x);
	int Max = maxIn3(v1.x, v2.x, v3.x);
	Bresenham_Line(Min, v1.y, Max, v1.y, ren);
}

void TriangleFill2(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	float a = (float)(v3.x - v1.x) / (v3.y - v1.y);
	float b = (float)(v3.x - v2.x) / (v3.y - v2.y);
	float X_LEFT = v1.x;
	float X_RIGHT = v2.x;
	for (int i = v1.y; i <= v3.y; i++)
	{
		X_LEFT += a;
		X_RIGHT += b;
		Bresenham_Line(int(X_LEFT + 0.5), i, int(X_RIGHT + 0.5), i, ren);
	}
}

void TriangleFill3(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	float a = (float)(v2.x - v1.x) / (v2.y - v1.y);
	float b = (float)(v3.x - v1.x) / (v3.y - v1.y);
	float X_LEFT = v1.x;
	float X_RIGHT = v1.x;
	for (int i = v1.y; i <= v3.y; i++)
	{
		X_LEFT += a;
		X_RIGHT += b;
		Bresenham_Line(int(X_LEFT + 0.5), i, int(X_RIGHT + 0.5), i, ren);
	}
}

void TriangleFill4(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	float b = (float)(v3.x - v1.x) / (v3.y - v1.y);
	float a = (float)(v2.x - v1.x) / (v2.y - v1.y);
	float X_LEFT = v1.x;
	float X_RIGHT = v1.x;
	for (int i = v1.y; i <= v3.y; i++)
	{
		X_LEFT += a;
		X_RIGHT += b;
		Bresenham_Line(int(X_LEFT + 0.5), i, int(X_RIGHT + 0.5), i, ren);
	}
}

void TriangleFill(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	ascendingSort(v1, v2, v3);
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	if (v1.y == v2.y && v2.y == v3.y) {
		TriangleFill1(v1, v2, v3, ren, fillColor);
		return;
	}
	if (v1.y == v2.y && v2.y < v3.y) {
		TriangleFill2(v1, v2, v3, ren, fillColor);
		return;
	}
	if (v1.y < v2.y && v3.y == v2.y) {
		TriangleFill3(v1, v2, v3, ren, fillColor);
		return;
	}
	if (v1.y < v2.y && v2.y < v3.y) {
		TriangleFill4(v1, v2, v3, ren, fillColor);
		return;
	}
	return;
}
//======================================================================================================================
//===================================CIRCLE - RECTANGLE - ELLIPSE=======================================================
bool isInsideCircle(int xc, int yc, int R, int x, int y)
{
	if (pow(x - xc, 2) + pow(y - yc, 2) <= R*R)
		return true;
	return false;
}

void FillIntersection(int x1, int y1, int x2, int y2, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	int xmin = x1 > x2 ? x2 : x1;
	int xmax = x1 + x2 - xmin;
	for (int x = xmin; x <= xmax; x++)
	{
		if (isInsideCircle(xc, yc, R, x, y1) == true)
		{
			SDL_RenderDrawPoint(ren, x, y1);
		}
	}
}

void FillIntersectionRectangleCircle(Vector2D vTopLeft, Vector2D vBottomRight, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	int y = vTopLeft.y;
	while (y <= vBottomRight.y)
	{
		FillIntersection(vTopLeft.x, y, vBottomRight.x, y, xc, yc, R, ren, fillColor);
		y++;
	}
}

void RectangleFill(Vector2D vTopLeft, Vector2D vBottomRight, SDL_Renderer *ren, SDL_Color fillColor)
{
	int y = vTopLeft.y;
	while (y != vBottomRight.y)
	{
		Midpoint_Line(vTopLeft.x, y, vBottomRight.x, y, ren);
		y++;
		cout << "ok";
	}
}

void put4line(int xc, int yc, int x, int y, SDL_Renderer *ren, SDL_Color fillColor)
{
	Bresenham_Line(xc + x, yc - y, xc - x, yc - y, ren);
	Bresenham_Line(xc + x, yc + y, xc - x, yc + y, ren);
	Bresenham_Line(xc - y, yc + x, xc + y, yc + x, ren);
	Bresenham_Line(xc - y, yc - x, xc + y, yc - x, ren);
}

void CircleFill(int xc, int yc, int R, SDL_Renderer *ren, SDL_Color fillColor)
{
	int x = R, y = 0;
	int p = 1 - R;
	put4line(xc, yc, x, y, ren,fillColor);
	while (y <= x)
	{
		if (p <= 0)
		{
			p += 2 * y + 3;
		}
		else
		{
			p += 2 * (y - x) + 5;
			x--;
		}
		y++;
		put4line(xc, yc, x, y, ren,fillColor);
	}
}

void FillIntersectionEllipseCircle(int xcE, int ycE, int a, int b, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	// Area 1
	int x, y, a2, b2, p;
	x = 0;
	y = b;
	a2 = a*a;
	b2 = b*b;
	{
		FillIntersection(xcE + x, ycE - y, xcE - x, ycE - y, xc, yc, R, ren, fillColor);
		FillIntersection(xcE + x, ycE + y, xcE - x, ycE + y, xc, yc, R, ren, fillColor);
	}
	p = (b2 - (a2*b) + (0.25*a2) + 0.5);
	while (pow(x, 2)*(a2 + b2) <= pow(a, 4)) {
		if (p <= 0)
		{
			p += b2*(2 * x + 3);
		}
		else
		{
			p += b2*(2 * x + 3) + a2*(2 - 2 * y);
			y--;
		}
		x++;
		//2line
		FillIntersection(xcE + x, ycE - y, xcE - x, ycE - y, xc, yc, R, ren, fillColor);
		FillIntersection(xcE + x, ycE + y, xcE - x, ycE + y, xc, yc, R, ren, fillColor);
	}	
	// Area 2
	y = 0;
	x = a;
	p = 2 * ((float)a2 / b2) - 2 * a + 1;
	while (((float)a2 / b2)*y <= x)
	{
		//2line intersect
		FillIntersection(xcE + x, ycE - y, xcE - x, ycE - y, xc, yc, R, ren, fillColor);
		FillIntersection(xcE + x, ycE + y, xcE - x, ycE + y, xc, yc, R, ren, fillColor);

		//
		if (p<0)
		{
			p = p + 2 * ((float)a2 / b2)*(2 * y + 3);
		}
		else
		{
			p = p - 4 * x + 2 * ((float)a2 / b2)*(2 * y + 3);
			x = x - 1;
		}
		y = y + 1;
	}
}

void FillIntersectionTwoCircles(int xc1, int yc1, int R1, int xc2, int yc2, int R2,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	int x = R1, y = 0;
	int p = 1 - R1;
	{
		FillIntersection(xc1 + x, yc1 - y, xc1 - x, yc1 - y, xc2, yc2, R2, ren,fillColor);
		FillIntersection(xc1 + x, yc1 + y, xc1 - x, yc1 + y, xc2, yc2, R2, ren,fillColor);
		FillIntersection(xc1 - y, yc1 + x, xc1 + y, yc1 + x, xc2, yc2, R2, ren, fillColor);
		FillIntersection(xc1 - y, yc1 - x, xc1 + y, yc1 - x, xc2, yc2, R2, ren, fillColor);
	}
	while (y <= x)
	{
		if (p <= 0)
		{
			p += 2 * y + 3;
		}
		else
		{
			p += 2 * (y - x) + 5;
			x--;
		}
		y++;
		{
			FillIntersection(xc1 + x, yc1 - y, xc1 - x, yc1 - y, xc2, yc2, R2, ren, fillColor);
			FillIntersection(xc1 + x, yc1 + y, xc1 - x, yc1 + y, xc2, yc2, R2, ren, fillColor);
			FillIntersection(xc1 - y, yc1 + x, xc1 + y, yc1 + x, xc2, yc2, R2, ren, fillColor);
			FillIntersection(xc1 - y, yc1 - x, xc1 + y, yc1 - x, xc2, yc2, R2, ren, fillColor);
		}	
	}
}
