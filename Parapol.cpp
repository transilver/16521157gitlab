#include "Parapol.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	int p = 1-A;
	int x = 0;
	int y = 0;
	Draw2Points(xc, yc, x, y, ren);
	while (x <= A)
	{
		if (p <= 0) {
			p += 2 *x+3;
		}
		else {
			p += 2 * x + 3 - 2 * A;
			y = y + 1;
		}
		x = x + 1;
		Draw2Points(xc, yc, x, y, ren);
	}
	p = 2*A - 1;
	while ((x < 400) && (x > -400))
	{
		if (p <= 0) {
			p += 4*A;
		}
		else {
			p += 4*A-4*x+4;
			x = x + 1;
		}
		y--;
		Draw2Points(xc, yc, x, y, ren);
	}
}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	int p = 1 - A;
	int x = 0;
	int y = 0;
	Draw2Points(xc, yc, x, y, ren);
	while (x <= A)
	{
		if (p <= 0) {
			p += 2 * x + 3;
		}
		else {
			p += 2 * x + 3 - 2 * A;
			y = y - 1;
		}
		x = x + 1;
		Draw2Points(xc, yc, x, y, ren);
	}
	p = 2 * A - 1;
	while ((x < 400) && (x > -400))
	{
		if (p <= 0) {
			p += 4 * A;
		}
		else {
			p += 4 * A - 4 * x - 4;
			x = x + 1;
		}
		y--;
		Draw2Points(xc, yc, x, y, ren);
	}
}
