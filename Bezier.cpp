#include "Bezier.h"
#include "Line.h"
#include <iostream>
using namespace std;

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	double x, y, bx=p1.x, by=p1.y;
	for (double i = 0; i <= 1; i += 0.001)
	{
		double u = 1 - i;
		x = pow(u, 2)*p1.x + 2*u*i*p2.x + pow(i, 2)*p3.x;
		y = pow(u, 2)*p1.y + 2*u*i*p2.y + pow(i, 2)*p3.y;
		Midpoint_Line(bx, by, x, y, ren);
		bx = x, by = y;
	}
	Midpoint_Line(bx, by, p3.x, p3.y, ren);
}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	double x, y, bx = p1.x, by = p1.y;
	for (double i = 0; i <= 1; i += 0.001)
	{
		double u = 1 - i;
		x = pow(u, 3)*p1.x + 3*pow(u, 2)*i*p2.x + 3*pow(i, 2)*u*p3.x + pow(i, 3)*p4.x;
		y = pow(u, 3)*p1.y + 3*pow(u, 2)*i*p2.y + 3*pow(i, 2)*u*p3.y + pow(i, 3)*p4.y;
		Midpoint_Line(bx, by, x, y, ren);
		bx = x, by = y;
	}
	Midpoint_Line(bx, by, p4.x, p4.y, ren);
}