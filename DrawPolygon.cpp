#include "DrawPolygon.h"
#include <iostream>
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	double GBD = M_PI / 2;
	double DoLech = 2 * M_PI / 3;
	int *x = new int[3];
	int *y = new int[3];
	double PHI = GBD;
	for (int i = 0; i < 3; i++)
	{
		x[i] = xc + int(R*cos(PHI)+0.5);
		y[i] = yc - int(R*sin(PHI)+0.5);
		PHI += DoLech;
	}
	for (int i = 0; i < 3; i++)
	{
		Bresenham_Line(x[i], y[i], x[(i + 1) % 3], y[(i + 1) % 3], ren);
	}
	return;
}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	double GBD = M_PI / 4;
	double DoLech = 2 * M_PI / 4;
	int *x = new int[4];
	int *y = new int[4];
	double PHI = GBD;
	for (int i = 0; i < 4; i++)
	{
		x[i] = xc + int(R*cos(PHI) + 0.5);
		y[i] = yc - int(R*sin(PHI) + 0.5);
		PHI += DoLech;
	}
	for (int i = 0; i < 4; i++)
	{
		Bresenham_Line(x[i], y[i], x[(i + 1) % 4], y[(i + 1) % 4], ren);
	}
	return;
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	double GBD = M_PI / 2;
	double DoLech = 2 * M_PI / 5;
	int *x = new int[5];
	int *y = new int[5];
	double PHI = GBD;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(PHI) + 0.5);
		y[i] = yc - int(R*sin(PHI) + 0.5);
		PHI += DoLech;
	}
	for (int i = 0; i < 5; i++)
	{
		Bresenham_Line(x[i], y[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
	return;
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	double GBD = M_PI / 2;
	double DoLech = 2 * M_PI / 6;
	int *x = new int[6];
	int *y = new int[6];
	double PHI = GBD;
	for (int i = 0; i < 6; i++)
	{
		x[i] = xc + int(R*cos(PHI) + 0.5);
		y[i] = yc - int(R*sin(PHI) + 0.5);
		PHI += DoLech;
	}
	for (int i = 0; i < 6; i++)
	{
		Bresenham_Line(x[i], y[i], x[(i + 1) % 6], y[(i + 1) % 6], ren);
	}
	return;
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	double GBD = M_PI / 2;
	double DoLech = 2 * M_PI / 5;
	int *x = new int[5];
	int *y = new int[5];
	double PHI = GBD;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(PHI) + 0.5);
		y[i] = yc - int(R*sin(PHI) + 0.5);
		PHI += DoLech;
	}
	for (int i = 0; i < 5; i++)
	{
		Midpoint_Line(x[i], y[i], x[(i + 2) % 5], y[(i + 2) % 5], ren);
	}
	return;
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	double GBD = M_PI / 2;
	double DoLech = 2 * M_PI / 5;
	int *x = new int[5];
	int *y = new int[5];
	int *xnho = new int[5];
	int *ynho = new int[5];
	int rnho = R*sin(M_PI / 10) / sin(7 * M_PI / 10);
	double PHI = GBD;
	double PHInho = GBD + M_PI / 5;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(PHI) + 0.5);
		y[i] = yc - int(R*sin(PHI) + 0.5);
		xnho[i] = xc + int(rnho*cos(PHInho) + 0.5);
		ynho[i] = yc - int(rnho*sin(PHInho) + 0.5);
		PHI += DoLech;
		PHInho += DoLech;
	}
	for (int i = 0; i < 5; i++)
	{
		Midpoint_Line(x[i], y[i], xnho[i], ynho[i], ren);
		Midpoint_Line(xnho[i], ynho[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
	return;
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	double GBD = M_PI / 2;
	double DoLech = 2 * M_PI / 8;
	int *x = new int[8];
	int *y = new int[8];
	int *xnho = new int[8];
	int *ynho = new int[8];
	int rnho = R*sin(M_PI / 8) / sin(6 * M_PI / 8);
	double PHI = GBD;
	double PHInho = GBD + M_PI / 8;
	for (int i = 0; i < 8; i++)
	{
		x[i] = xc + int(R*cos(PHI) + 0.5);
		y[i] = yc - int(R*sin(PHI) + 0.5);
		xnho[i] = xc + int(rnho*cos(PHInho) + 0.5);
		ynho[i] = yc - int(rnho*sin(PHInho) + 0.5);
		PHI += DoLech;
		PHInho += DoLech;
	}
	for (int i = 0; i < 8; i++)
	{
		Midpoint_Line(x[i], y[i], xnho[i], ynho[i], ren);
		Midpoint_Line(xnho[i], ynho[i], x[(i + 1) % 8], y[(i + 1) % 8], ren);
	}
	return;
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	double GBD = startAngle;
	double DoLech = 2 * M_PI / 5;
	int *x = new int[5];
	int *y = new int[5];
	int *xnho = new int[5];
	int *ynho = new int[5];
	int rnho = R*sin(M_PI / 10) / sin(7 * M_PI / 10);
	double PHI = GBD;
	double PHInho = GBD + M_PI / 5;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(PHI) + 0.5);
		y[i] = yc - int(R*sin(PHI) + 0.5);
		xnho[i] = xc + int(rnho*cos(PHInho) + 0.5);
		ynho[i] = yc - int(rnho*sin(PHInho) + 0.5);
		PHI += DoLech;
		PHInho += DoLech;
	}
	for (int i = 0; i < 5; i++)
	{
		Bresenham_Line(x[i], y[i], xnho[i], ynho[i], ren);
		Bresenham_Line(xnho[i], ynho[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
	return;
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	double gbd = M_PI / 2;
	while (r > 1)
	{
		DrawStarAngle(xc, yc, r, gbd, ren);
		r =int (r*sin(M_PI / 10) / sin(7 * M_PI / 10)+0.5);
		gbd += M_PI;
	}
}
