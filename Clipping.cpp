#include "Clipping.h"
#include <iostream>

RECT CreateWindow(int l, int r, int t, int b)
{
    RECT rect;
    rect.Left = l;
    rect.Right = r;
    rect.Top = t;
    rect.Bottom = b;

    return rect;
}

CODE Encode(RECT r, Vector2D P)
{
    CODE c = 0;
    if (P.x < r.Left)
        c = c|LEFT;
    if (P.x > r.Right)
        c = c|RIGHT;
    if (P.y < r.Top)
        c = c|TOP;
    if (P.y > r.Bottom)
        c = c|BOTTOM;
    return c;
}

int CheckCase(int c1, int c2)
{
    if (c1 == 0 && c2 == 0)
        return 1;
    if (c1 != 0 && c2 != 0 && c1&c2 != 0)
        return 2;
    return 3;
}

int CohenSutherland(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	Q1 = P1;
	Q2 = P2;
	int c1 = Encode(r, Q1);
	int c2 = Encode(r, Q2);
	if (CheckCase(c1, c2) == 1) return 1;
	while (CheckCase(c1, c2) == 3)
	{
		ClippingCohenSutherland(r, Q1, Q2);
		c1 = Encode(r, Q1);
		c2 = Encode(r, Q2);
	}
	if (CheckCase(c1, c2) == 2) 
	{
		Q1 = { 0,0 };
		Q2 = { 0,0 };
	}
	return 1;
}

void ClippingCohenSutherland(RECT r, Vector2D &P1, Vector2D &P2)
{
	if (Encode(r, P1) == 0)
	{
		Vector2D temp = P1;
		P1 = P2;
		P2 = temp;
	}
	int dx = P2.x - P1.x;
	int dy = P2.y - P1.y;
	if (dx == 0)
	{
		P1.y = P1.y > r.Top ? P1.y : r.Top;
		P1.y = P1.y < r.Bottom ? P1.y : r.Bottom;
		return;
	}
	if (dy == 0)
	{
		P1.x = P1.x > r.Left ? P1.x : r.Left;
		P1.x = P1.x < r.Right ? P1.x : r.Right;
		return;
	}
	float m = float(dy) / dx;
	int x, y;
	if (Encode(r, P1) & LEFT)
	{
		x = r.Left;
		y = P1.y + m*(x - P1.x);
	} else
	if (Encode(r, P1) & RIGHT)
	{
		x = r.Right;
		y = P1.y + m*(x - P1.x);
	} else
	if (Encode(r, P1) & TOP)
	{
		y = r.Top;
		x = P1.x + (y - P1.y)/m;
	} else
	if (Encode(r, P1) & BOTTOM)
	{
		y = r.Bottom;
		x = P1.x + (y - P1.y)/m;
	}
	P1.x = x;
	P1.y = y;
}

int SolveNonLinearEquation(int p, int q, float &t1, float &t2)
{
    if (p == 0)
    {
        if (q < 0)
            return 0;
        return 1;
    }

    if (p > 0)
    {
        float t=(float)q/p;
        if(t2<t)
            return 1;
        if(t<t1)
            return 0;
        t2 = t;
        return 1;
    }

    float t=(float)q/p;
    if(t2<t)
        return 0;
    if(t<t1)
        return 1;
    t1 = t;
    return 1;
}

int LiangBarsky(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	int dx = P2.x - P1.x;
	int dy = P2.y - P1.y;
	int xmax = r.Right;
	int ymax = r.Bottom;
	int xmin = r.Left;
	int ymin = r.Top;
	float t1=0, t2=1;
	if (SolveNonLinearEquation(-dx, P1.x - xmin, t1, t2)==1)
		if (SolveNonLinearEquation(dx, xmax - P1.x, t1, t2)==1)
			if (SolveNonLinearEquation(-dy, P1.y - ymin, t1, t2)==1)
				if (SolveNonLinearEquation(dy, ymax - P1.y, t1, t2)==1)
				{
					Q1.x = P1.x + t1*dx;
					Q1.y = P1.y + t1*dy;
					Q2.x = P1.x + t2*dx;
					Q2.y = P1.y + t2*dy;
				}
	return 1;
}
