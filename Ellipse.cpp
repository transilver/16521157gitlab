#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1
	float p, a2, b2;
	int x, y;
	a2 = pow(a, 2);
	b2 = pow(b, 2);
	x = 0;
	y = b;

	p = 2 * ((float)b2 / a2) - (2 * b) + 1;

	//ve nhanh thu 1(tu tren xuong )
	while (((float)b2 / a2)*x <= y)
	{
		Draw4Points(xc, yc, x, y, ren);
		if (p<0)
		{
			p = p + 2 * ((float)b2 / a2)*(2 * x + 3);
		}
		else {
			p = p - 4 * y + 2 * ((float)b2 / a2)*(2 * x + 3);
			y--;
		}
		x++;
	}
	// Area 2
	y = 0;
	x = a;
	p = 2 * ((float)a2 / b2) - 2 * a + 1;	
	while (((float)a2 / b2)*y <= x)
	{
		Draw4Points(xc, yc, x, y, ren);
		if (p<0)
		{
			p = p + 2 * ((float)a2 / b2)*(2 * y + 3);
		}
		else
		{
			p = p - 4 * x + 2 * ((float)a2 / b2)*(2 * y + 3);
			x = x - 1;
		}
		y = y + 1;
	}
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1
	int x, y, a2, b2, p;
	x = 0;
	y = b;
	a2 = a*a;
	b2 = b*b;
	Draw4Points(xc, yc, x, y, ren);
	p = (b2 - (a2*b) + (0.25*a2)+0.5);
	while (pow(x,2)*(a2+b2)<=pow(a,4))	{
		if (p<=0)
		{
			p += b2*(2 * x + 3);
		}
		else
		{
			p += b2*(2 * x + 3) + a2*(2 - 2 * y);
			y--;
		}
		x++;
		Draw4Points(xc, yc, x, y, ren);
	}
	// Area 2
	y = 0;
	x = a;
	p = 2 * ((float)a2 / b2) - 2 * a + 1;
	while (((float)a2 / b2)*y <= x)
	{
		Draw4Points(xc, yc, x, y, ren);
		if (p<0)
		{
			p = p + 2 * ((float)a2 / b2)*(2 * y + 3);
		}
		else
		{
			p = p - 4 * x + 2 * ((float)a2 / b2)*(2 * y + 3);
			x = x - 1;
		}
		y = y + 1;
	}

}
